#!/usr/bin/env bash

declare -x POD="mariadb"
declare -x DB_CONTAINER="db"
declare -x KOALA_CONTAINER="koala-lms"

podman stop -t=1 "${DB_CONTAINER}"
podman stop -t=1 "${KOALA_CONTAINER}"

podman rm "${DB_CONTAINER}"
podman rm "${KOALA_CONTAINER}"

podman pod rm "${POD}"
podman pod create --name="${POD}" --share net -p 8080:8080

podman create --detach --pod="${POD}" \
              --name="${DB_CONTAINER}" \
              --hostname="${DB_CONTAINER}" \
              -e MYSQL_ROOT_PASSWORD=koala \
              -e MYSQL_DATABASE=koala \
              -e MYSQL_USER=koala \
              -e MYSQL_PASSWORD=koala \
              --add-host "${KOALA_CONTAINER}":127.0.0.1 \
              --add-host "${DB_CONTAINER}":127.0.0.1 \
              --add-host "${POD}":127.0.0.1 \
              mariadb:latest

podman create --detach --pod="${POD}"  \
              --name="${KOALA_CONTAINER}" \
              --hostname="${KOALA_CONTAINER}" \
              -e FIXTURE="./fixtures/sample-fr.json" \
              --expose 8080 \
              --add-host "${KOALA_CONTAINER}":127.0.0.1 \
              --add-host "${DB_CONTAINER}":127.0.0.1 \
              --add-host "${POD}":127.0.0.1 \
              -v settings.py:/koala_lms/lms/local_settings.py \
              registry.gitlab.com/koala-lms/lms:devel

podman start "${DB_CONTAINER}"
podman start "${KOALA_CONTAINER}"
