image: python:alpine

before_script:
  - apk add --no-cache build-base
  - pip3 install -r ./requirements.txt  # Install project requirements

stages:
  # Build
  - test_build
  - build
  # Test
  - test
  # Deploy
  - test_deploy
  - deploy
  # Production
  - production

##################################################
##                 Build jobs                   ##
##################################################

# Build pages only on develop
pages:
  stage: build
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
  script:
    - apk add make
    - pip3 install -r ./docs/requirements.txt
    - make -C docs html
  after_script:
    - mv docs/build/html/ public/
  artifacts:
    paths:
      - public
    expire_in: "5 min"
  rules:
    - if: '$CI_COMMIT_BRANCH == "develop"'
      when: always
    - when: never

# Always build a package
.package:
  stage: build
  script:
    - echo "No action in script. Override if you wish."
  after_script:
    - apk add --no-cache sassc gettext
    - CSS_DIR="${PKG_NAME}/static/${PKG_NAME}/scss"; sassc --style compressed "${CSS_DIR}/${PKG_NAME}".scss "${CSS_DIR}/${PKG_NAME}".css
    - django-admin compilemessages
    - python3 setup.py sdist bdist_wheel
  artifacts:
    name: $CI_PROJECT_NAME-$CI_COMMIT_REF_NAME
    paths:
      - dist
  rules:
    - when: always

package:
  extends: .package


##################################################
##                  Test jobs                   ##
##################################################

#######################
## The syntax linter ##
#######################

syntax-linter:
  stage: test
  script:
    - pip3 install prospector
    - prospector -i "${PKG_NAME}"/demo
  allow_failure: true
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: always
      allow_failure: false
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "master"'
      when: always
      allow_failure: false
    - if: $CI_COMMIT_TAG
      when: always
      allow_failure: false
    - when: always
      allow_failure: true

#########################
## The security linter ##
#########################

security-linter:
  stage: test
  script:
    - pip3 install bandit
    - bandit -r "${PKG_NAME}" -x "*/tests/*","*/demo/*"
  allow_failure: true
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: always
      allow_failure: false
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "master"'
      when: always
      allow_failure: false
    - if: $CI_COMMIT_TAG
      when: always
      allow_failure: false
    - when: always
      allow_failure: true


################
## Unit tests ##
################
tests:
  stage: test
  script:
    - pip install coverage
    - cd "${PKG_NAME}"/demo && coverage run manage.py test "${PKG_NAME}"
    - coverage report
  rules:
    - when: always

##################################################
##             Test deploy jobs                 ##
##################################################
.pypi:
  image: python:alpine
  dependencies:
    - package
  variables:
    GIT_STRATEGY: none
    TWINE_NON_INTERACTIVE: 1
  before_script:
    - apk add --no-cache python3-dev libffi-dev openssl-dev build-base
    - pip3 install twine

pypi:test:
  stage: test_deploy
  extends: .pypi
  variables:
    TWINE_USERNAME: $TEST_PYPI_USERNAME
    TWINE_PASSWORD: $TEST_PYPI_PASSWORD
  script:
    - twine upload --repository-url https://test.pypi.org/legacy/ dist/*
  rules:
    - if: $CI_COMMIT_TAG
      when: on_success

##################################################
##                 Deploy jobs                  ##
##################################################
pypi:
  stage: deploy
  extends: .pypi
  variables:
    TWINE_USERNAME: $PYPI_USERNAME
    TWINE_PASSWORD: $PYPI_PASSWORD
  script:
    - twine upload dist/*
  rules:
    - if: $CI_COMMIT_TAG
      when: on_success

##################################################
##               Production jobs                ##
##################################################
deploy_demo:
  image: alpine
  stage: production
  variables:
    GIT_STRATEGY: none
  before_script:
    - apk add --no-cache curl
  script:
    - curl -X POST -F token="${DEPLOY_LMS_TOKEN}" -F ref=develop https://gitlab.com/api/v4/projects/11707844/trigger/pipeline
  rules:
    - if: '$CI_COMMIT_BRANCH == "develop"'
      when: on_success
